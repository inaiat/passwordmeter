**Avaliação de password**

Requisitos mínimos para rodar a aplicação:

    JAVA 8
    
Como rodar:

    git clone https://bitbucket.org/inaiat/passwordmeter.git
    cd passwordmeter/

    Linux:
        
        ./mvnw spring-boot:run

    Windows:
    
        mvnw.cmd spring-boot:run
        
    Abrir o navegador apontando para http://localhost:8080/
