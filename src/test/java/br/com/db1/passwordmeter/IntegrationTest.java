package br.com.db1.passwordmeter;

import br.com.db1.passwordmeter.web.model.ComplexityModel;
import lombok.val;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {


	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	public void requestOk() {
		val body = this.testRestTemplate.getForEntity("/score?password=NibiruIshere", ComplexityModel.class);
		Assert.assertEquals( Integer.valueOf(42), body.getBody().getScore());
	}

	@Test
	public void requestFail() {
		val body = this.testRestTemplate.getForEntity("/score", ComplexityModel.class);
		Assert.assertEquals( 400, body.getStatusCodeValue());
	}


}
