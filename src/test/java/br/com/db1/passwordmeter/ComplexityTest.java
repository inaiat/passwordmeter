package br.com.db1.passwordmeter;

import br.com.db1.passwordmeter.service.PasswordMeterService;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

public class ComplexityTest {

    @Test
    public void strongPassword() {
        val target = new PasswordMeterService();
        val response = target.applyAll("Telefone@2017");

        assertEquals (Integer.valueOf(52), response.get("Number of Characters").getScore());
        assertEquals (Integer.valueOf(24), response.get("Uppercase Letters").getScore());
        assertEquals (Integer.valueOf(12), response.get("Lowercase Letters").getScore());
        assertEquals (Integer.valueOf(16), response.get("Numbers").getScore());
        assertEquals (Integer.valueOf(6), response.get("Symbols").getScore());
        assertEquals (Integer.valueOf(8), response.get("Middle Numbers or Symbols").getScore());
        assertEquals (Integer.valueOf(10), response.get("Requirements").getScore());
        assertEquals (Integer.valueOf(3), response.get("Repeat Characters").getCount());
        assertEquals (Integer.valueOf(-1), response.get("Repeat Characters").getScore());
    }


    @Test
    public void weakPassword() {
        val target = new PasswordMeterService();
        val response = target.applyAll("TELEFONE");
        assertEquals (Integer.valueOf(32), response.get("Number of Characters").getScore());
        assertEquals (Integer.valueOf(0), response.get("Uppercase Letters").getScore());
        assertEquals (Integer.valueOf(0), response.get("Lowercase Letters").getScore());
        assertEquals (Integer.valueOf(0), response.get("Numbers").getScore());
        assertEquals (Integer.valueOf(0), response.get("Symbols").getScore());
        assertEquals (Integer.valueOf(0), response.get("Middle Numbers or Symbols").getScore());
        assertEquals (Integer.valueOf(0), response.get("Requirements").getScore());
        assertEquals (Integer.valueOf(-14), response.get("Consecutive Uppercase Letters").getScore());

    }

    @Test
    public void weakPassword2() {
        val target = new PasswordMeterService();
        val response = target.applyAll("telefone");

        assertEquals (Integer.valueOf(32), response.get("Number of Characters").getScore());
        assertEquals (Integer.valueOf(0), response.get("Uppercase Letters").getScore());
        assertEquals (Integer.valueOf(0), response.get("Lowercase Letters").getScore());
        assertEquals (Integer.valueOf(0), response.get("Numbers").getScore());
        assertEquals (Integer.valueOf(0), response.get("Symbols").getScore());
        assertEquals (Integer.valueOf(0), response.get("Middle Numbers or Symbols").getScore());
        assertEquals (Integer.valueOf(0), response.get("Requirements").getScore());
        assertEquals (Integer.valueOf(-14), response.get("Consecutive Lowercase Letters").getScore());

    }

    @Test
    public void sequenceTest() {
        val target = new PasswordMeterService();
        val response = target.applyAll("TELEFONEabcxcba");
        assertEquals (Integer.valueOf(-3), response.get("Sequential Letters (3+)").getScore());
    }


    @Test
    public void symbolTest() {
        val target = new PasswordMeterService();
        val response1 = target.applyAll("@!#$");
        assertEquals (Integer.valueOf(24), response1.get("Symbols").getScore());
        assertEquals (Integer.valueOf(4), response1.get("Middle Numbers or Symbols").getScore());
    }

    @Test
    public void consecutiveTest() {
        val target = new PasswordMeterService();
        val response1 = target.applyAll("telefoneABCabc");
        assertEquals (Integer.valueOf(-18), response1.get("Consecutive Lowercase Letters").getScore());
    }


    @Test
    public void repeatedCharTest() {
        val target = new PasswordMeterService();
        val response1 = target.applyAll("Abracadabra");
        val response2 = target.applyAll("telefone");
        assertEquals (Integer.valueOf(-3), response1.get("Repeat Characters").getScore());
        assertEquals (Integer.valueOf(-2), response2.get("Repeat Characters").getScore());
    }


    @Test
    public void scoreTotal() {
        val target = new PasswordMeterService();
        val display = target.applyAll("4304&6345");
        display.forEach( (k,v) -> {
            System.out.print(k);
            System.out.print("=");
            System.out.println(v);
        });
        val response1 = target.totalScore("4304&6345");
        val response2 = target.totalScore( "SAFt4jitgnkoguiafs");
        assertEquals (Integer.valueOf(71), response1);
        assertEquals (Integer.valueOf(95), response2);


    }
}
