package br.com.db1.passwordmeter.service.rule;

import lombok.val;

import java.util.regex.Pattern;

public class ConditionalLetterCaseRule implements PasswordRule {

    private final Pattern pattern;

    private Integer counter = 0;

    public ConditionalLetterCaseRule(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public PasswordMeterResult execute(String password) {
        counter = 0;
        val matcher = pattern.matcher(password);
        while (matcher.find())
            counter++;

        if (counter==0) {
            return PasswordMeterResult.builder().count(0).score(0).build();
        } else {
            val result = (password.length() - counter) * 2;
            return PasswordMeterResult.builder().count(counter).score(result).build();
        }
    }

}
