package br.com.db1.passwordmeter.service.rule;

import lombok.val;

import java.util.regex.Pattern;

public class SequenceRule implements PasswordRule {

    private final Pattern pattern;

    public SequenceRule(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public PasswordMeterResult execute(String password) {
        val matcher = pattern.matcher(password);
        int count = 0;
        while (matcher.find()) {
            val result =  matcher.group(0);
            val array = result.chars().toArray();
            int wordCount = 0;
            for (int i = 0; i < array.length; i++) {
                if (i>0) {
                    if (array[i-1]==array[i]-1) wordCount++;
                }
            }
            if (wordCount>1) {
                count++;
            }
        }
        if (count>0) {
            return PasswordMeterResult.builder().count(count).score(count * -3).build();
        } else {
            return PasswordMeterResult.builder().count(0).score(0).build();
        }
    }
}
