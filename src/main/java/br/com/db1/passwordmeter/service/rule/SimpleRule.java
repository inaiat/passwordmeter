package br.com.db1.passwordmeter.service.rule;

import lombok.Builder;
import lombok.Data;
import lombok.val;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleRule implements PasswordRule {

    private final Pattern pattern;
    private final Function<ScoreFunction, Integer> scoreFunction;

    public SimpleRule(Pattern pattern, Function<ScoreFunction, Integer> scoreFunction) {
        this.pattern = pattern;
        this.scoreFunction = scoreFunction;
    }

    @Override
    public PasswordMeterResult execute(String password) {
        val counter = countMatcher(pattern.matcher(password));

        return PasswordMeterResult.builder()
                .count(counter)
                .score(scoreFunction.apply(ScoreFunction.builder().count(counter).length(password.length()).build())
                ).build();
    }

    private static Integer countMatcher(Matcher matcher) {
        int count = 0;
        while (matcher.find())
            count++;
        return count;
    }

    @Data
    @Builder
    public static class ScoreFunction {
        private Integer count;
        private Integer length;
    }

}
