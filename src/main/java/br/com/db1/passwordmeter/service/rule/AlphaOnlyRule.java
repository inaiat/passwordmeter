package br.com.db1.passwordmeter.service.rule;

import lombok.val;

import java.util.regex.Pattern;

public class AlphaOnlyRule implements PasswordRule {

    private final Pattern pattern;

    public AlphaOnlyRule(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public PasswordMeterResult execute(String password) {
        val matcher = pattern.matcher(password);
        int count = 0;
        while (matcher.find())
            count++;

        if (count == password.length()) {
            val score = count * -1;
            return PasswordMeterResult.builder().count(count).score(score).build();
        } else {
            return PasswordMeterResult.builder().count(0).score(0).build();
        }
    }
}
