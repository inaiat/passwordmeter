package br.com.db1.passwordmeter.service.rule;

import lombok.val;

import java.util.regex.Pattern;

public class MiddleNumberOrSymbolRule implements PasswordRule {
    private static final Pattern PATTERN = Pattern.compile("(?!([A-Z]|[a-z])).");

    @Override
    public PasswordMeterResult execute(String password) {
        Integer calc = 0;
        if (password.length()>2) {
            val middle = password.substring(1,password.length()-1);
            val matcher = PATTERN.matcher(middle);
            while (matcher.find())
                calc++;
        }
        return PasswordMeterResult.builder().count(calc).score(calc * 2).build();

    }
}
