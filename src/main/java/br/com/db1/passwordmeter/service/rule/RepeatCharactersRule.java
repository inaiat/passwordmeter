package br.com.db1.passwordmeter.service.rule;

import lombok.val;

import java.util.stream.Collectors;

public class RepeatCharactersRule implements PasswordRule {
    @Override
    public PasswordMeterResult execute(String password) {
        val list = password.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        Double score = 0.0;
        Integer repeatChar = 0;
        Integer uniqueChar;
        for (int i=0; i < list.size(); i++) {
            boolean charExist =false;
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i) == list.get(j) && i != j) {
                    charExist = true;
                    score = Double.sum(score, Math.abs( (double) list.size() / (j - i)));
                }
            }
            if (charExist) {
                repeatChar++;
                uniqueChar = list.size() - repeatChar;
                if (uniqueChar>0) {
                    score = Double.valueOf(Math.ceil(score/uniqueChar));
                } else {
                    score= Double.valueOf(Math.ceil(score));
                }
            }
        }

        return PasswordMeterResult.builder().count(repeatChar).score(score.intValue() * -1).build();


    }
}
