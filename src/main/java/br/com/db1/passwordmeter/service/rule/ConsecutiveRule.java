package br.com.db1.passwordmeter.service.rule;

import lombok.val;

import java.util.regex.Pattern;

public class ConsecutiveRule implements PasswordRule {

    private final Pattern pattern;

    public ConsecutiveRule(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public PasswordMeterResult execute(String password) {
        int count = 0;
        val matcher = pattern.matcher(password);
        while (matcher.find()) {
            count += matcher.group(0).length() -1;
        }
        return PasswordMeterResult.builder().count(count).score(count * -2).build();
    }
}
