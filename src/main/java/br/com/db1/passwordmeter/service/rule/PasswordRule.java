package br.com.db1.passwordmeter.service.rule;

public interface PasswordRule {

    PasswordMeterResult execute(String password);

}
