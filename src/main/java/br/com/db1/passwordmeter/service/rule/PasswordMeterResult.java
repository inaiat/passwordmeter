package br.com.db1.passwordmeter.service.rule;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PasswordMeterResult {

    private Integer count;
    private Integer score;

}
