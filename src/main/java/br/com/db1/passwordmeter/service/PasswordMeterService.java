package br.com.db1.passwordmeter.service;


import br.com.db1.passwordmeter.service.rule.*;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class PasswordMeterService {

    private static final PasswordRule UPPER_CASE_LETTER = new ConditionalLetterCaseRule(Pattern.compile("[A-Z]"));
    private static final PasswordRule LOWER_CASE_LETTER = new ConditionalLetterCaseRule(Pattern.compile("[a-z]"));
    private static final PasswordRule NUMBERS =
            new SimpleRule(Pattern.compile("[0-9]"), score -> score.getCount() * 4);
    private static final PasswordRule SYMBOLS =
            new SimpleRule(
                    Pattern.compile("(([0-9])|([A-Z])|([a-z]))"),
                    calcFuncion -> (calcFuncion.getLength() - calcFuncion.getCount()) * 6);

    private final Map<String, PasswordRule> additionsRules =
            Collections.unmodifiableMap(new HashMap<String, PasswordRule>() {
                {

                    // Aditions
                    put("Number of Characters", password -> PasswordMeterResult.builder().count(0).score(password.length() * 4).build());

                    put("Uppercase Letters", UPPER_CASE_LETTER);

                    put("Lowercase Letters", LOWER_CASE_LETTER);

                    put("Numbers", NUMBERS);

                    put("Symbols", SYMBOLS);

                    put("Middle Numbers or Symbols", new MiddleNumberOrSymbolRule());

                    put("Requirements", password -> {
                        Integer requirements = 0;
                        Integer score = 0;
                        requirements += (password.length() > 7 ? 1 : 0);
                        requirements += UPPER_CASE_LETTER.execute(password).getCount() > 0 ? 1 : 0;
                        requirements += LOWER_CASE_LETTER.execute(password).getCount() > 0 ? 1 : 0;
                        requirements += NUMBERS.execute(password).getScore() > 0 ? 1 : 0;
                        requirements += SYMBOLS.execute(password).getScore() > 0 ? 1 : 0;
                        if (requirements > 3) score = requirements * 2;
                        return PasswordMeterResult.builder().count(requirements).score(score).build();
                    });
                }
            });


    private final Map<String, PasswordRule> deductionsRules =
            Collections.unmodifiableMap(new HashMap<String, PasswordRule>() {
                {
                    // Deductions
                    put("Letters Only", new AlphaOnlyRule(Pattern.compile("([A-Z])|([a-z])")));
                    put("Numbers Only", new AlphaOnlyRule((Pattern.compile("([0-9])"))));
                    put("Repeat Characters", new RepeatCharactersRule());
                    put("Consecutive Uppercase Letters", new ConsecutiveRule(Pattern.compile("([A-Z]){2,}")));
                    put("Consecutive Lowercase Letters", new ConsecutiveRule(Pattern.compile("([a-z]){2,}")));
                    put("Consecutive Numbers", new ConsecutiveRule(Pattern.compile("([0-9]){2,}")));
                    put("Sequential Letters (3+)", new SequenceRule(Pattern.compile("([aA-zZ]){3,}")));
                    put("Sequential Numbers (3+)", new SequenceRule(Pattern.compile("([0-9]{3,})")));
                    put("Sequential Symbols (3+)", new SequenceRule(Pattern.compile("(\\)|\\!|\\@|\\#|\\$|\\%|\\^|\\&|\\*|\\(|\\)){3,}")));


                }
            });

    private final Map<String, PasswordRule> allRules;

    public PasswordMeterService() {
        this.allRules = new HashMap<>();
        this.allRules.putAll(additionsRules);
        this.allRules.putAll(deductionsRules);
    }

    public Map<String, PasswordMeterResult> applyAll(String password) {
        return allRules
                .entrySet()
                .stream()
                .collect(
                        Collectors.toMap(Map.Entry::getKey, v -> v.getValue().execute(password))
                );
    }

    public Integer totalScore(String password) {
        Integer additions = additionsRules
                .entrySet()
                .stream()
                .map(e -> e.getValue().execute(password))
                .mapToInt(PasswordMeterResult::getScore).sum();

        Integer deductions = deductionsRules
                .entrySet()
                .stream()
                .map(e -> e.getValue().execute(password))
                .mapToInt(PasswordMeterResult::getScore).sum();

        return additions + deductions;

    }


}
