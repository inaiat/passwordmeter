package br.com.db1.passwordmeter.web.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ComplexityModel {

    private Integer score;

}
