package br.com.db1.passwordmeter.web.rest;

import br.com.db1.passwordmeter.service.PasswordMeterService;
import br.com.db1.passwordmeter.web.model.ComplexityModel;
import lombok.val;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PasswordMeterController {

    private final PasswordMeterService passwordMeterService;

    public PasswordMeterController(PasswordMeterService passwordMeterService) {
        this.passwordMeterService = passwordMeterService;
    }

    @RequestMapping("/score")
    public ComplexityModel home(@RequestParam(value = "password") String password) {
        val result = passwordMeterService.totalScore(password);
        return ComplexityModel.builder().score(result).build();
    }

}
