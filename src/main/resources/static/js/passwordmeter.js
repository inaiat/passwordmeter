angular.module('passwordmeter', [])
.directive('progressBar', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var watchFor = attrs.progressBarWatch;
      scope.$watch(watchFor, function(val) {
        element.attr( "class", "progress-bar " + val.level );
      })
    }
  }
})
.controller('passwordmeter', function($http, $scope) {
	var self = this;

	$scope.reset = function() {
	    $scope.label = "Muito Curta";
	    $scope.level = "progress-bar-danger"
        $scope.progress = {"score": 0, "label": $scope.level};
    }

    $scope.reset();

	$scope.checkComplexity = function() {
	    if ($scope.password.length<1) {
                $scope.reset();
	    } else {
            $http.get('score/', {params: { password: $scope.password }}).then(function(response) {
                $scope.complexity = response.data;
                var score = $scope.complexity.score;
                var label = "Muito Fraca";
                var level = " progress-bar-danger";
                if (score < 0) { score = 0 }
                if (score > 100) { score = 100; }

                if (score >= 0 && score < 20) { label = "Muito Fraca"; }
                if (score >= 20 && score < 40) { label = "Fraca"; }
                if (score >= 40 && score < 60) { label = "Boa"; level = "progress-bar-warning"; }
                if (score >= 60 && score < 80) { label = "Forte"; level = "progress-bar-success"; }
                if (score >= 80 && score <= 100) { label = "Muito Forte"; level = "progress-bar-info"; }

                $scope.label = label;
                $scope.level = level;
                $scope.progress = {"score": score, "level": level};
            })
        }
	}
});
